<?php
// $Id$

/**
 * Template to display flickrnodes information.
 * 
 * Fields available:
 *   $title:       string
 *   $description: string
 *   $photos:      array
 */

drupal_add_css(drupal_get_path('module', 'flickrnodes') . "/flickrnodes.css");

?>

<h3><?php echo $title; ?></h3>
<?php echo $description; ?>

<div class="flickrnodes_photos">
  <?php foreach ($photos as $photo): ?>
    <div class="flickrnodes_photo">
	    <a href="<?php echo $photo['linkURL']; ?>"><img src="<?php echo $photo['imageURL']; ?>" alt="<?php echo $photo['title']; ?>" /></a>   
      <h4><a href="<?php echo $photo['linkURL']; ?>"><?php echo $photo['title']; ?></a></h4>
	    <?php echo $photo['description']; ?>
	  </div>
  <?php endforeach; ?>
</div>
