<?php
// $Id$

/**
 * Template to display flickrnodes information.
 * 
 * Fields available:
 *   $photosets:  array
 */

drupal_add_css(drupal_get_path('module', 'flickrnodes') . "/flickrnodes.css");

?>

<div class="flickrnodes_photosets">
  <?php foreach ($photosets as $photoset): ?> 
    <div class="flickrnodes_photoset">   
      <a href="<?php echo base_path() . drupal_get_path_alias($_GET['q']) . '/photoset/' . $photoset['id']; ?>"><img src="<?php echo $photoset['imageURL']; ?>" alt="<?php echo $photoset['imageAlt']; ?>" /></a>
	    <h3><a href="<?php echo base_path() . drupal_get_path_alias($_GET['q']) . '/photoset/' . $photoset['id']; ?>"><?php echo $photoset['title']; ?></a></h3>
	    <?php echo $photoset['description']; ?>    
    </div>
  <?php endforeach; ?>
</div>
