<?php
// $Id$

/**
 * Implementation of hook_settings(). Creates a form for the admin page.
 *
 * @return System form object.
 */
function flickrnodes_admin_settings()
{
  $form['flickrnodes_api_key'] = 
    array('#type'          => 'textfield',
          '#title'         => t('Flickr API Key'),
          '#required'      => true,
          '#default_value' => variable_get('flickrnodes_api_key', ''),
          '#description'   => t('Flickr API Key'));
 
  $form['flickrnodes_api_secret'] = 
    array('#type'          => 'textfield',
          '#title'         => t('Flickr API Shared Secret'),
          '#required'      => true,
          '#default_value' => variable_get('flickrnodes_api_secret', ''),
          '#description'   => t("Flickr API shared secret."));
  
  return system_settings_form($form);
}

/**
 * Implementation of hook_validate(). Validates the admin form.
 *
 * @param object $form       The form object.
 * @param array  $form_state The form state.
 */
function flickrnodes_admin_settings_validate($form, &$form_state) 
{
  $api_key    = trim($form_state['values']['flickrnodes_api_key']);
  $api_secret = trim($form_state['values']['flickrnodes_api_secret']);

  if (!preg_match('/^[A-Fa-f\d]{32}$/', $api_key)) {
    form_set_error('flickrnodes_api_key', t('This does not appear to be a Flickr API key.'));
  }
  
  if (!preg_match('/^[A-Fa-f\d]{16}$/', $api_secret)) {
    form_set_error('flickrnodes_api_secret', t('This does not appear to be a Flickr API shared secret.'));
  }
}

/**
 * Implements hook_submit(). Saves the admin information.
 *
 * @param object $form       The form object.
 * @param array  $form_state The form state.
 */
function flickrnodes_admin_settings_submit($form, &$form_state) 
{
  $form_state['values']['flickrnodes_api_key']    = trim($form_state['values']['flickrnodes_api_key']);
  $form_state['values']['flickrnodes_api_secret'] = trim($form_state['values']['flickrnodes_api_secret']);

  system_settings_form_submit($form, $form_state);
}
