<?php
// $Id$

/**
 * Template to display flickrnodes paging links.
 * 
 * Fields available:
 *   $base_url:   string
 *   $page:       integer
 *   $totalPages: integer
 */
?>

<?php 

$items = array();

// Unless this is the first page add 'first' and 'previous' links.
if ($page != 1) {
	$items[] = array('class' => 'pager-first',
                   'data'  => '<a href="' . $base_url . '">' . t('« first') . '</a>');
	$items[] = array('class' => 'pager-previous',
                   'data'  => '<a href="' . $base_url . ($page - 1) . '">' . t('‹ previous') . '</a>');
}

$start  = $page - 2;
$stop   = $page + 2;
if ($start < 1) {  
  $stop  = $stop - $start + 1; 
  $start = 1;
}
if ($stop > $totalPages) {
	$start = ($start + $totalPages - $stop > 0) ? $start + $totalPages - $stop : 1;
	$stop  = $totalPages;
}


// Add in an elipsis if we are skipping pages at the start.
if ($start != 1) {
	$items[] = array('class' => 'pager-ellipsis',
                   'data' => '…');
}

for ($i = $start; $i <= $stop; $i++) {
	if ($i == $page) {
		$items[] = array('class' => 'pager-current',
		                 'data'  => $i);
	} else {
		$items[] = array('class' => 'page-item',
		                 'data'  => '<a href="' . $base_url . $i . '">' . "$i</a>");
	}
}

// Add in an elipsis if we are skipping pages at the end.
if ($stop != $totalPages) {
  $items[] = array('class' => 'pager-ellipsis',
                   'data' => '…');
}

// Unless this is the last page add 'next' and 'last' links.
if ($page != $totalPages) {
  $items[] = array('class' => 'pager-next',
                   '<a href="' . $base_url . ($page + 1) . '">' . t('next ›') . '</a>');
  $items[] = array('class' => 'pager-next',
                   '<a href="' . $base_url . $totalPages . '">' . t('last »') . '</a>');
}

echo theme('item_list', $items, null, 'ul', array('class' => 'pager')); 

?>
