<?php
// $Id$

/**
 * Template to display flickrnodes information.
 * 
 * Fields available:
 *   $title:       string
 *   $description: string
 */

drupal_add_css(drupal_get_path('module', 'flickrnodes') . "/flickrnodes.css");

?>

<div class="flickrnodes_photosets">
  <div class="flickrnodes_photoset">   
	  <h3><a href="<?php echo base_path() . drupal_get_path_alias($_GET['q']) . '/group'; ?>"><?php echo $title; ?></a></h3>
	  <?php echo $description; ?>    
  </div>
</div>
